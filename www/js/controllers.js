angular.module('starter.controllers', [])

.controller('MusicCtrl', ['$scope', 'MediaManager', function($scope, MediaManager) {
    var urlprefix = '/android_asset/www/audio/';

    $scope.dynamicTrack = {};

    $scope.tracks = [
        {
            url: urlprefix + '03 - Land Of Confusion.mp3',
            artist: 'Genesis',
            title: 'Land of Confusion'
        },
        {
        url: 'https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3',
		artist: 'Somebody',
		title: 'Song name',
		art: 'http://cellntell.com/cellntell-mobile-endpoint/wp-content/uploads/2017/02/cell_logo.png'
		},
        {
            url: urlprefix + '02 - Tonight. Tonight. Tonight.mp3',
            artist: 'Genesis',
            title: 'Tonight. Tonight. Tonight'
        }
    ];

    $scope.stopPlayback = function() {
		alert('index');
        MediaManager.stop();
		
    };
    $scope.playTrack = function(index) {
		console.log("sdsdsdsdds"+index);
        $scope.dynamicTrack = $scope.tracks[index];

        $scope.togglePlayback = !$scope.togglePlayback;    
    };
	// stop any track before leaving current view
    $scope.$on('$ionicView.beforeLeave', function() {
        MediaManager.stop();
    });
/*	 $scope.myCallback = function() {
            MediaManager.play();
        };*/
}]);
